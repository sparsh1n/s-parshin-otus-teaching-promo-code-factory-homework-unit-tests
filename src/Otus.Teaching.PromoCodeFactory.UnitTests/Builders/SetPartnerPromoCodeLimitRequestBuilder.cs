﻿using Otus.Teaching.PromoCodeFactory.WebHost.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace Otus.Teaching.PromoCodeFactory.UnitTests.Builders
{
    public static class SetPartnerPromoCodeLimitRequestBuilder
    {
        public static SetPartnerPromoCodeLimitRequest CreateSetPartnerPromoCodeLimitRequest()
        {
            return new SetPartnerPromoCodeLimitRequest()
            {
                Limit = 100,
                EndDate = DateTime.Now.AddMonths(3)
            };
        }
    }
}
