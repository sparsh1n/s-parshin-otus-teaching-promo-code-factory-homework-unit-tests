﻿using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using System;
using System.Collections.Generic;
using System.Text;

namespace Otus.Teaching.PromoCodeFactory.UnitTests.Builders
{
    public static class PartnerBuilder
    {
        public static Partner CreatePartner()
        {
            var partner = new Partner()
            {
                Id = Guid.Parse("7d994823-8226-4273-b063-1a95f3cc1df8"),
                Name = "Суперигрушки",
                IsActive = true,
                NumberIssuedPromoCodes = 10,
                PartnerLimits = new List<PartnerPromoCodeLimit>(),
                
            };

            return partner;
        }

        public static Partner SetIsActive(this Partner partner, bool isActive)
        {
            partner.IsActive = isActive;

            return partner;
        }

        public static Partner SetNumberIssuedPromoCodes(this Partner partner, int numberIssuedPromoCodes)
        {
            partner.NumberIssuedPromoCodes = numberIssuedPromoCodes;  
            
            return partner;
        }

        public static Partner AddActivePromoCodeLimit(this Partner partner)
        {
            partner.PartnerLimits.Add(

                    new PartnerPromoCodeLimit()
                    {
                        Id = Guid.NewGuid(),
                        CreateDate = DateTime.Now.AddMonths(-1),
                        EndDate = DateTime.Now.AddMonths(2),
                        Limit = 100,
                        Partner = partner,
                        PartnerId = partner.Id
                    }
                );

            return partner;
        }

        public static Partner AddExpiredPromoCodeLimit(this Partner partner)
        {
            partner.PartnerLimits.Add(

                    new PartnerPromoCodeLimit()
                    {
                        Id = Guid.NewGuid(),
                        CreateDate = DateTime.Now.AddMonths(-4),
                        EndDate = DateTime.Now.AddMonths(-1),
                        Limit = 100,
                        Partner = partner,
                        PartnerId = partner.Id
                    }
                );

            return partner;
        }
    }
}
