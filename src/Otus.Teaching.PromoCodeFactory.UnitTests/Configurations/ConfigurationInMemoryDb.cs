﻿using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.DataAccess;
using Otus.Teaching.PromoCodeFactory.DataAccess.Data;
using Otus.Teaching.PromoCodeFactory.DataAccess.Repositories;
using System;
using System.Collections.Generic;
using System.Text;

namespace Otus.Teaching.PromoCodeFactory.UnitTests.Configurations
{
    public static class ConfigurationInMemoryDb
    {
        public static IServiceCollection GetServiceCollection(IConfigurationRoot configuration, IServiceCollection serviceCollection = null)
        {
            if (serviceCollection == null)
            {
                serviceCollection = new ServiceCollection();
            }
            serviceCollection
                .AddSingleton(configuration)
                .AddSingleton((IConfiguration)configuration)
                .AddScoped(typeof(IRepository<>), typeof(EfRepository<>))
                .AddScoped<IDbInitializer, EfDbInitializer>()
                .AddMemoryCache();
            return serviceCollection;
        }

        public static IServiceCollection ConfigureInMemoryContext(this IServiceCollection services)
        {
            var serviceProvider = new ServiceCollection()
                .AddEntityFrameworkInMemoryDatabase()
                .BuildServiceProvider();

            services.AddDbContext<DataContext>(options =>
            {
                options.UseInMemoryDatabase($"TestInMemoryDb_{Guid.NewGuid()}");
                options.UseInternalServiceProvider(serviceProvider);
            });

            return services;
        }


    }
}
