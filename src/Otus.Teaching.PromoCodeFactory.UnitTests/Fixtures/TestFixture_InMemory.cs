﻿using Microsoft.AspNetCore.Hosting;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Otus.Teaching.PromoCodeFactory.DataAccess;
using Otus.Teaching.PromoCodeFactory.DataAccess.Data;
using Otus.Teaching.PromoCodeFactory.UnitTests.Configurations;
using Otus.Teaching.PromoCodeFactory.WebHost;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Text;

namespace Otus.Teaching.PromoCodeFactory.UnitTests.Fixtures
{
    public class TestFixture_InMemory : IDisposable
    {
        public IServiceProvider ServiceProvider { get; set; }
        public IServiceCollection ServiceCollection { get; set; }

        public TestFixture_InMemory()
        {
            var builder = new ConfigurationBuilder();
            var configuration = builder.Build();
            ServiceCollection = ConfigurationInMemoryDb.GetServiceCollection(configuration);
            ServiceProvider = GetServiceProvider();
        }

        private IServiceProvider GetServiceProvider()
        {
            var serviceProvider = ServiceCollection
                .ConfigureInMemoryContext()
                .BuildServiceProvider();
            serviceProvider.GetRequiredService<IDbInitializer>().InitializeDb();
            return serviceProvider;
        }


        public void Dispose()
        {
        }
    }

}
