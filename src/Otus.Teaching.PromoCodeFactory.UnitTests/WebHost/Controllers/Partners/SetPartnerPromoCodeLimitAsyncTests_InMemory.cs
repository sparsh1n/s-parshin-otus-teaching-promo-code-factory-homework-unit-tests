﻿using FluentAssertions;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.DependencyInjection;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using Otus.Teaching.PromoCodeFactory.UnitTests.Builders;
using Otus.Teaching.PromoCodeFactory.UnitTests.Fixtures;
using Otus.Teaching.PromoCodeFactory.WebHost.Controllers;
using Otus.Teaching.PromoCodeFactory.WebHost.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace Otus.Teaching.PromoCodeFactory.UnitTests.WebHost.Controllers.Partners
{
    public class SetPartnerPromoCodeLimitAsyncTests_InMemory : IClassFixture<TestFixture_InMemory>
    {
        private TestFixture_InMemory _testFixtireInMemory;
        private IRepository<Partner> _partnerRepository;
        private PartnersController _partnersController;
        
        public SetPartnerPromoCodeLimitAsyncTests_InMemory(TestFixture_InMemory testFixtireInMemory)
        {
            _testFixtireInMemory = testFixtireInMemory;
            _partnerRepository = testFixtireInMemory.ServiceProvider.GetRequiredService<IRepository<Partner>>();
            _partnersController = new PartnersController(_partnerRepository);
        }

        private SetPartnerPromoCodeLimitRequest CreateSetPartnerPromoCodeLimitRequest() =>
            SetPartnerPromoCodeLimitRequestBuilder.CreateSetPartnerPromoCodeLimitRequest();

        /// <summary>
        /// Нужно убедиться, что сохранили новый лимит в базу данных (это нужно проверить Unit-тестом)
        /// </summary>
        [Fact] 
        public async Task SetPartnerPromoCodeLimitAsync_NewLimitSavedInDB_Success()
        {
            //Arrange

            //достаем партнера из базы
            var partner = (await _partnerRepository.GetAllAsync()).FirstOrDefault();
            //сохраняем количество его лимитов
            var limitsCount = partner.PartnerLimits.Count;
            
            var request = CreateSetPartnerPromoCodeLimitRequest();

            //Act
            
            //вызываем метод
            var result = await _partnersController.SetPartnerPromoCodeLimitAsync(partner.Id, request);
            
            //получаем Id нового лимита из результата работы метода
            var newLimitId = Guid.Parse((result as CreatedAtActionResult).RouteValues.GetValueOrDefault("limitId").ToString());

            //получаем еще раз партнера из базы
            var reloadedPartner = (await _partnerRepository.GetAllAsync()).FirstOrDefault(t => t.Id.Equals(partner.Id));
            //и лимит по вернувшемуся айдишнику
            var newLimit = reloadedPartner.PartnerLimits.FirstOrDefault(t => t.Id.Equals(newLimitId));

            //Assert

            //результат выполнение метода - успехЪ
            result.Should().BeAssignableTo<CreatedAtActionResult>();
            //партнер есть
            reloadedPartner.Should().NotBeNull();
            //количество лимитов увеличилось
            reloadedPartner.PartnerLimits.Count.Should().Equals(limitsCount + 1);
            //лимит не null
            newLimit.Should().NotBeNull();
        }
    }
}
