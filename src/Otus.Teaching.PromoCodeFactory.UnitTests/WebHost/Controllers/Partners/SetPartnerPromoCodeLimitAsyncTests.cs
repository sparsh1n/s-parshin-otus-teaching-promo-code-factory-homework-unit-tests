﻿using System;
using System.Linq;
using System.Collections.Generic;
using AutoFixture;
using AutoFixture.AutoMoq;
using FluentAssertions;
using Microsoft.AspNetCore.Mvc;
using Moq;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using Otus.Teaching.PromoCodeFactory.UnitTests.Builders;
using Otus.Teaching.PromoCodeFactory.WebHost.Controllers;
using Otus.Teaching.PromoCodeFactory.WebHost.Models;
using Xunit;

namespace Otus.Teaching.PromoCodeFactory.UnitTests.WebHost.Controllers.Partners
{
    public class SetPartnerPromoCodeLimitAsyncTests
    {
        private readonly Mock<IRepository<Partner>> _partnersRepositoryMock = new Mock<IRepository<Partner>>();
        private readonly PartnersController _partnersController;

        public SetPartnerPromoCodeLimitAsyncTests()
        {
            _partnersController = new PartnersController(_partnersRepositoryMock.Object);
        }

        private Partner CreatePartner() => PartnerBuilder.CreatePartner();
        private SetPartnerPromoCodeLimitRequest CreateSetPartnerPromoCodeLimitRequest() =>
            SetPartnerPromoCodeLimitRequestBuilder.CreateSetPartnerPromoCodeLimitRequest();


        /// <summary>
        /// Партнер не найден
        /// </summary>
        [Fact]
        public async void SetPartnerPromoCodeLimitAsync_PartnerIsNotFound_ReturnsNotFound()
        {
            // Arrange
            var partnerId = Guid.Empty;
            Partner partner = null;

            _partnersRepositoryMock.Setup(repo => repo.GetByIdAsync(partnerId))
                .ReturnsAsync(partner);
            var request = CreateSetPartnerPromoCodeLimitRequest();

            // Act
            var result = await _partnersController.SetPartnerPromoCodeLimitAsync(partnerId, request);

            // Assert
            result.Should().BeAssignableTo<NotFoundResult>();
        }

        /// <summary>
        /// Партнер не активен
        /// </summary>
        [Fact]
        public async void SetPartnerPromoCodeLimitAsync_PartnerIsNotActive_ReturnsBadRequest()
        {
            // Arrange
            var partner = CreatePartner().SetIsActive(false);
            _partnersRepositoryMock.Setup(repo => repo.GetByIdAsync(partner.Id))
                .ReturnsAsync(partner);

            var request = CreateSetPartnerPromoCodeLimitRequest();

            // Act
            var result = await _partnersController.SetPartnerPromoCodeLimitAsync(partner.Id, request);

            // Assert
            result.Should().BeAssignableTo<BadRequestObjectResult>();
        }

        /// <summary>
        /// Если партнеру выставляется лимит, то мы должны обнулить количество промокодов, которые партнер выдал NumberIssuedPromoCodes,
        /// </summary>
        [Fact]
        public async void SetPartnerPromoCodeLimitAsync_PartnerHaveActivePromoCodeLimit_NumberOfIssuedPromoCodesEqualsZero()
        {
            // Arrange
            var partner = CreatePartner()
                .SetIsActive(true).
                AddActivePromoCodeLimit();

            _partnersRepositoryMock.Setup(repo => repo.GetByIdAsync(partner.Id))
                .ReturnsAsync(partner);
            var request = CreateSetPartnerPromoCodeLimitRequest();

            // Act
            var result = await _partnersController.SetPartnerPromoCodeLimitAsync(partner.Id, request);

            // Assert
            result.Should().BeAssignableTo<CreatedAtActionResult>();
            partner.NumberIssuedPromoCodes.Should().Be(0);
        }

        /// <summary>
        /// Если предыдущий лимит закончился, то количество не обнуляется;
        /// </summary>
        [Fact]
        public async void SetPartnerPromoCodeLimitAsync_PartnerHaveNotActivePromoCodeLimit_NumberOfIssuedPromoCodesMoreThanZero()
        {
            // Arrange
            var partner = CreatePartner()
                .SetNumberIssuedPromoCodes(100)
                .AddExpiredPromoCodeLimit();

            _partnersRepositoryMock.Setup(repo => repo.GetByIdAsync(partner.Id))
                .ReturnsAsync(partner);
            var request = CreateSetPartnerPromoCodeLimitRequest();

            // Act
            var result = await _partnersController.SetPartnerPromoCodeLimitAsync(partner.Id, request);

            // Assert
            result.Should().BeAssignableTo<CreatedAtActionResult>();
            partner.NumberIssuedPromoCodes.Should().BeGreaterThan(0);
        }

        /// <summary>
        /// При установке лимита нужно отключить предыдущий активный лимит
        /// </summary>
        [Fact]
        public async void SetPartnerPromoCodeLimitAsync_PreviousActiveLimitSetCancelDate_CancelDateNotNull()
        {
            // Arrange
            var partner = CreatePartner()
                .AddActivePromoCodeLimit();
            var limit = partner.PartnerLimits.First();

            _partnersRepositoryMock.Setup(repo => repo.GetByIdAsync(partner.Id))
                .ReturnsAsync(partner);
            var request = CreateSetPartnerPromoCodeLimitRequest();

            // Act
            var result = await _partnersController.SetPartnerPromoCodeLimitAsync(partner.Id, request);

            // Assert
            result.Should().BeAssignableTo<CreatedAtActionResult>();
            limit.CancelDate.Should().NotBeNull();
        }

        /// <summary>
        /// Если предыдущий лимит истек по дате, то дата отмены не проставляется
        /// </summary>
        [Fact]
        public async void SetPartnerPromoCodeLimitAsync_PreviousLimitExpired_CancelDateIsNull()
        {
            // Arrange
            var partner = CreatePartner()
                .AddExpiredPromoCodeLimit();
            var limit = partner.PartnerLimits.First();

            _partnersRepositoryMock.Setup(repo => repo.GetByIdAsync(partner.Id))
                .ReturnsAsync(partner);
            var request = CreateSetPartnerPromoCodeLimitRequest();

            // Act
            var result = await _partnersController.SetPartnerPromoCodeLimitAsync(partner.Id, request);

            // Assert
            result.Should().BeAssignableTo<CreatedAtActionResult>();
            limit.CancelDate.Should().BeNull();
        }

        /// <summary>
        /// При установке нового лимита, он должен быть без CancelDate
        /// </summary>
        [Fact]
        public async void SetPartnerPromoCodeLimitAsync_NewActiveLimitShouldBeWithoutCancelDate_LimitIsNotNull()
        {
            // Arrange
            var partner = CreatePartner().AddActivePromoCodeLimit();

            _partnersRepositoryMock.Setup(repo => repo.GetByIdAsync(partner.Id))
                .ReturnsAsync(partner);
            var request = CreateSetPartnerPromoCodeLimitRequest();

            // Act
            var result = await _partnersController.SetPartnerPromoCodeLimitAsync(partner.Id, request);
            var limit = partner.PartnerLimits.FirstOrDefault(x => x.CancelDate is null);

            // Assert
            result.Should().BeAssignableTo<CreatedAtActionResult>();
            limit.Should().NotBeNull();
            limit.CancelDate.Should().BeNull();
        }


        /// <summary>
        /// При установке лимит должен быть больше 0
        /// </summary>
        /// 
        [Theory]
        [InlineData(0)]
        [InlineData(-1)]
        public async void SetPartnerPromoCodeLimitAsync_LimitEqualsZero_ReturnsBadRequest(int limitValue)
        {
            // Arrange
            var partner = CreatePartner();

            _partnersRepositoryMock.Setup(repo => repo.GetByIdAsync(partner.Id))
                .ReturnsAsync(partner);

            var request = CreateSetPartnerPromoCodeLimitRequest();
            request.Limit = limitValue;

            // Act
            var result = await _partnersController.SetPartnerPromoCodeLimitAsync(partner.Id, request);

            // Assert
            result.Should().BeAssignableTo<BadRequestObjectResult>();
        }

        /// <summary>
        /// Метод успешно работает и добавляет промокод
        /// </summary>

        [Fact]
        public async void SetPartnerPromoCodeLimitAsync_LimitMoreThanZero_ReturnsCreatedAtActionResult()
        {
            // Arrange
            var partner = CreatePartner().AddActivePromoCodeLimit();

            _partnersRepositoryMock.Setup(repo => repo.GetByIdAsync(partner.Id))
                .ReturnsAsync(partner);
            var request = CreateSetPartnerPromoCodeLimitRequest();

            // Act
            var result = await _partnersController.SetPartnerPromoCodeLimitAsync(partner.Id, request);

            // Assert
            result.Should().BeAssignableTo<CreatedAtActionResult>();
        }
    }
}